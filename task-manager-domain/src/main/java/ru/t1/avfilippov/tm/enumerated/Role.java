package ru.t1.avfilippov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private final String displayName;

    Role(@Nullable final String displayName) {
        this.displayName = displayName;
    }

}
