package ru.t1.avfilippov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class JmsLoggerProducer {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Queue destination;

    @NotNull
    private final MessageProducer messageProducer;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @SneakyThrows
    public JmsLoggerProducer(final IPropertyService propertyService) {
        this.propertyService = propertyService;
        broker.addConnector(propertyService.getJmsURL());
        broker.start();
        connection = connectionFactory.createConnection();
        this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        this.destination = session.createQueue(QUEUE);
        this.messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        @NotNull final TextMessage textMessage = session.createTextMessage(text);
        messageProducer.send(textMessage);
    }

    public void send(@NotNull final OperationEvent operationEvent) {
        executorService.submit(() -> sync(operationEvent));
    }

    @SneakyThrows
    private void sync(@NotNull final OperationEvent operationEvent) {
        @NotNull final Class<?> entityClass = operationEvent.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            operationEvent.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(operationEvent));
    }

}
