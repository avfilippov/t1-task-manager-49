package ru.t1.avfilippov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    int getSessionTimeout();

    @NotNull
    String getInitToken();

    @NotNull
    String getLiquibaseConfig();

    @NotNull
    String getJmsURL();

}
